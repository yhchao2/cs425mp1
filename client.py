import socket
import threading
import sys
import argparse
import os
import json
import re
from typing import List, Tuple

SERVER_LIST = [
    ("fa23-cs425-7601.cs.illinois.edu", 8005, 1),
    ("fa23-cs425-7602.cs.illinois.edu", 8005, 2),
    ("fa23-cs425-7603.cs.illinois.edu", 8005, 3),
    ("fa23-cs425-7604.cs.illinois.edu", 8005, 4),
    ("fa23-cs425-7605.cs.illinois.edu", 8005, 5),
    ("fa23-cs425-7606.cs.illinois.edu", 8005, 6),
    ("fa23-cs425-7607.cs.illinois.edu", 8005, 7),
    ("fa23-cs425-7608.cs.illinois.edu", 8005, 8),
    ("fa23-cs425-7609.cs.illinois.edu", 8005, 9),
    ("fa23-cs425-7610.cs.illinois.edu", 8005, 10)
]

# LOCAL DEBUG SERVER LIST
LOCAL_SERVER_LIST = [
    ("localhost", 8001, 1),
    ("localhost", 8002, 2),
    ("localhost", 8003, 3),
    ("localhost", 8004, 4),
    ("localhost", 8005, 5),
    ("localhost", 8006, 6),
    ("localhost", 8007, 7),
    ("localhost", 8008, 8),
    ("localhost", 8009, 9),
    ("localhost", 8010, 10)
]

# -1 = NO OUTPUT
# 0 = ONLY TOTAL_LINES
# 1 = TOTAL_LINES + SERVER_LINES (ERRORS ARE IGNORED)
# 2 = TOTAL_LINES + SERVER_LINES + SERVER_OUTPUT
DEFAULT_VERBOSITY = 2
MAX_BUFFER_SIZE = 1024 # 1 KB


def print_count_result(server_output : List[dict], server_list : List[Tuple[str, int, int]], verbosity : int = 1) -> None:
    """
    Print the result of the grep command with the -c flag.

    Args:
        server_output (List[dict]): The output from the server.
        server_list (List[Tuple[str, int, int]]): The basic information about the servers.
        verbosity (int, optional): The logging verbosity. Defaults to 1.
    """

    total_lines = 0
    success_count = 0
    for server_result in server_output:
        if server_result["success"]:
            print(server_result["result"])
            total_lines += int(server_result["result"])
            success_count += 1

    if verbosity == 0:
        print(total_lines)
    elif verbosity == 1:
        print(f"Total lines: {total_lines}")

        for index, server_result in enumerate(server_output):
            _, _, server_number = server_list[index]

            if server_result["success"]:
                print(f"Server {server_number}, lines: {server_result['result']}")
    else:
        print(f"##### Total lines: {total_lines} (from {len(server_output)} servers)   #####")
        print(f"##### Server (Success : {success_count}, Total : {len(server_output)}) #####")
        for index, server_result in enumerate(server_output):
            server_host, server_port, server_number = server_list[index]

            status = "🟢" if server_result["success"] else "🔴"
            print(f"--- Server {server_number} @ {server_host}:{server_port} {status} ---")
            print(server_result["result"])


def print_result(server_output : List[dict], server_list : List[Tuple[str, int, int]]) -> None:
    """
    Print the result of the grep command.

    Args:
        server_output (List[dict]): The output from the server.
        server_list (List[Tuple[str, int, int]]): The basic information about the servers.
    """

    # VERBOSITY doesn't matter here
    success_count = 0
    for server_result in server_output:
        if server_result["success"]:
            success_count += 1

    print(f"##### Server (Success : {success_count}, Total : {len(server_output)}) #####")

    for index, server_result in enumerate(server_output):
        server_host, server_port, server_number = server_list[index]

        status = "🟢" if server_result["success"] else "🔴"
        print(f"--- Server {server_number} @ {server_host}:{server_port} {status} ---")
        print(server_result["result"])

def get_server_filename(filename : str, server_number : int) -> str:
    """
    Get the filename that the server should use.

    Args:
        filename (str): the base filename
        server_number (int): the server number

    Returns:
        str: the filename that the server should use
    """
    try:
        base_name, extension = os.path.splitext(filename)
        return f"{base_name}{server_number}{extension}"
    except Exception as exception:
        print(f"Invalid filename { filename }: { exception }")
        sys.exit(1)

def get_command_flags(command : str) -> str:
    """
    Get the flags from the command.

    Args:
        command (str): the command

    Returns:
        str: the flags
    """
    pattern = r'(-{1,2}\w+)'
    flags = re.findall(pattern, command)
    return flags


def connect_and_send (
    server_tuple : Tuple[str, int],
    command_object : dict,
    server_output : dict
) -> None:
    """
    Args:
        server_tuple (Tuple[str, int]): the server's ip and port
        command (str): the grep command (without the filename)
        filename (str): the filename to run the grep command on
        server_output (dict):
            the dictionary to store the result of the grep command
            it has the form {"success" : bool, "result" : str}
    """
    try:
        client_socket = socket.socket()
        client_socket.connect(server_tuple)
        # send the command in blocks
        encoded_command = json.dumps(command_object).encode()
        for i in range(0, len(encoded_command), MAX_BUFFER_SIZE):
            client_socket.send(encoded_command[i : i + MAX_BUFFER_SIZE])
        client_socket.shutdown(socket.SHUT_WR)

        data = b''
        # receive data by chunks
        while True:
            chunk = client_socket.recv(MAX_BUFFER_SIZE)
            if not chunk:
                break
            data += chunk

        data = data.decode()
        server_output.update(json.loads(data))
    except Exception as exception:
        server_output.update({
            "success" : False,
            "result" : f"Error connecting to server: {exception}"
        })
    finally:
        client_socket.close()


def connect_and_send_all(server_list : List[Tuple[str, int, int]], command_objects : List[dict]) -> List[dict]:
    """
    Connect to all servers and send the command.

    Args:
        server_list (List[Tuple[str, int, int]]): The basic information about the servers.
        command_objects (List[dict]): The commands to send to the servers.

    Returns:
        List[dict]: The output from the servers.
    """
    all_threads = []
    server_output = [{} for _ in range(len(server_list))]

    for index, (server_host, server_port, _) in enumerate(server_list):
        child_thread = threading.Thread(
            target = connect_and_send,
            args = (
                (server_host, server_port), command_objects[index], server_output[index]
            )
        )
        child_thread.start()
        all_threads.append(child_thread)

    for child_thread in all_threads:
        child_thread.join()

    return server_output

def grep_command(server_list : List[Tuple[str, int, int]], command : str, filename : str, verbosity : int = 1) -> List[dict]:
    # command = input("please enter grep: ")
    """
    Execute the grep command on the servers.

    Args:
        server_list (List[Tuple[str, int, int]]): The basic information about the servers.
        command (str): The grep command (without the filename).
        filename (str): The filename to run the grep command on.

    Returns:
        List[dict]: The output from the servers.
    """
    server_output = connect_and_send_all(server_list, [{
        "operation" : "query",
        "command" : command,
        "filename" : get_server_filename(filename, server_number)
    } for _, _, server_number in server_list])

    flags = get_command_flags(command)

    # check if the command has the -c flag
    if verbosity != -1:
        if "-c" in flags or "--count" in flags:
            print_count_result(server_output, server_list, verbosity)
        else:
            print_result(server_output, server_list)

    return server_output

def main():
    """
    The main function.
    """
    parser = argparse.ArgumentParser(description="Client for CS425 MP1")
    parser.add_argument("--command", type=str, help="The grep command to run on the server. (don't include the filename)")
    parser.add_argument("--filename", type=str, help="The filename to run the grep command on.")
    parser.add_argument('--server', action='store_true', help='Run on the server.')
    parser.add_argument("--verbosity", type=int, help="The verbosity of the output. (0 = only total lines, 1 = total lines + server lines, 2 = total lines + server lines + server output)")

    args = parser.parse_args()

    command = args.command
    filename = args.filename

    if not command or not filename:
        parser.print_help()
        sys.exit(1)
    try:
        server_list = SERVER_LIST if args.server else LOCAL_SERVER_LIST
        verbosity = args.verbosity if args.verbosity is not None else DEFAULT_VERBOSITY
        grep_command(server_list, command, filename, verbosity)
    except Exception as exception:
        print("Error: " + str(exception))
        sys.exit(1)

if __name__ == "__main__":
    main()