import socket
import subprocess
import sys
import argparse
from typing import Optional
from time import sleep
import json
from client import get_command_flags

# TYPE ALIASES
Socket = socket.socket

# GLOBAL CONSTANTS
DEFAULT_LOCAL_HOST = 'localhost'
DEFAULT_SERVER_HOST = socket.gethostbyname(socket.gethostname())
PORT = 8005
MAX_CONNECTIONS = 10
START_MAX_RETRIES = 5
RETRY_INTERVAL = 1
MAX_BUFFER_SIZE = 1024 # 1 KB

def handle_query(command : str, filename : str) -> dict:
    """
    Handle a query. Return the result of the query.

    command : A grep command without the filename.
    filename : The filename to run the grep command on.
    """
    print(f"Received grep command: {command} {filename}")

    # Get flags
    flags = get_command_flags(command)

    # Run grep command
    try:
        process_result = subprocess.run(
            f"{command} {filename}",
            shell=True,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            universal_newlines=True,
            check=True
        )

        # print(f"Successfully ran grep command: {process_result.stdout}")
        if '-c' in flags or '--count' in flags:
            result = int(process_result.stdout)
        else:
            result = process_result.stdout

        return {
            "success" : True,
            "result" : result
        }
    except subprocess.CalledProcessError as exception:
        if exception.returncode == 1:
            if '-c' in flags or '--count' in flags:
                result = 0
            else:
                result = "No matches found."
            return {
                "success" : True,
                "result" : result
            }
        elif exception.returncode == 2:
            return {
                "success" : False,
                "result" : f"Invalid grep syntax: {exception}"
            }
        elif exception.returncode == 127:
            return {
                "success" : False,
                "result" : f"Command not found: {exception}"
            }

        print(f"Error running grep command: {exception}")
        return {
            "success" : False,
            "result" : f"Error running grep command: {exception}"
        }

def handle_write(filename : str, data : str) -> dict:
    """
    Handle a write. Write the command to the file.

    filename : The filename to write the file to.
    data : The data to write to the file.
    """
    print(f"Received write command to write to {filename}")

    try:
        with open(filename, "w", encoding = "utf-8") as file:
            file.write(data)
        return {
            "success" : True,
            "result" : "Successfully wrote to file."
        }
    except Exception as exception:
        print(f"Error writing to file: {exception}")
        return {
            "success" : False,
            "result" : f"Error writing to file: {exception}"
        }

def print_and_send(client_socket : Socket, message : dict) -> None:
    """
    Print a message and send it to the client.

    socket : The socket to send the message to.
    message : The message to print and send.
    """
    try:
        print(message["result"])
    except KeyError:
        print("Internal Server Error")

    try:
        client_socket.send(json.dumps(message).encode())
    except Exception as exception:
        print(f"Error sending data: {exception}")
        # No further error handling if we couldn't even send the message
        client_socket.send(json.dumps({
            "success" : False,
            "result" : "Internal Server Error"
        })).encode()



def handle_connection(server_socket : Socket, max_buffer_size : int = 1024) -> None:
    """
    Handle a connection. Receive command from client (Structured using JSON)

    server_socket : The server socket to accept connections from.
    max_buffer_size : The maximum buffer size to receive data from.

    Structure : {
        "operation" : "query",
        "command" : str, # without filename
        "filename" : str,
    } or {
        "operation" : "write",
        "filename" : str,
        "data" : str,
    }
    """
    try:
        client_socket, address = server_socket.accept()
    except Exception as exception:
        print(f"Error accepting connection: {exception}")
        return

    def print_and_send_error(message : str) -> None:
        print_and_send(client_socket, {
            "success" : False,
            "result" : message
        })

    with client_socket:
        print(f"Connection from: {address}")

        # Receive data from client in chunks

        data_binary = b''
        while True:
            chunk = client_socket.recv(max_buffer_size)
            if not chunk:
                break
            data_binary += chunk

        # Decode data
        data_plain = data_binary.decode()

        if not data_plain:
            print("Client disconnected.")
            return

        # Parse data into JSON, and get operation
        try:
            data_json = json.loads(data_plain)
            operation = data_json["operation"]
        except json.decoder.JSONDecodeError as exception:
            print_and_send_error(f"Invalid JSON format: {exception}")
            return
        except KeyError as exception:
            print_and_send_error(f"Invalid Operation Structure: {exception}")
            return

        # Execute command
        result = None

        # Query
        if operation == "query":
            try:
                command = data_json["command"]
                filename = data_json["filename"]
            except KeyError as exception:
                print_and_send_error(f"Invalid Query Structure: {exception}")
                return

            result = handle_query(command, filename)
        # Write
        elif operation == "write":
            try:
                filename = data_json["filename"]
                data = data_json["data"]
            except KeyError as exception:
                print_and_send_error(f"Invalid Write Structure: {exception}")
                return

            result = handle_write(filename, data)
        # Invalid operation
        else:
            print_and_send_error(f"Invalid operation: {operation}")
            return

        # Send result to client
        try:
            # try to send result in chunks (due to protocol package size limit)
            # print(f"Sending result: {result}")
            encoded_result = json.dumps(result).encode()

            for i in range(0, len(encoded_result), max_buffer_size):
                client_socket.send(encoded_result[i:i+max_buffer_size])

            # close connection (automatically done by with statement)
        except Exception as exception:
            print_and_send_error(f"Error sending data: {exception}")
            return


def set_up_socket(host : str, port : int, max_connections : int,) -> Optional[Socket]:
    """
    Set up the server socket. If it fails to set up, return None.
    Bind the socket to host:port and listen for connections with a maximum of max_connections.

    Args:
        host: The host to bind the socket to.
        port: The port to bind the socket to.
        max_connections: The maximum number of connections to listen for.
    """

    try:
        server_socket = Socket()
        server_socket.bind((host, port))
        server_socket.listen(max_connections)
    except Exception as exception:
        print("Error starting server: " + str(exception))
        return None

    return server_socket

def start_server(
    host : str, port : int,
    max_connections : int,
    max_retries : int = 5, retry_interval = 1
) -> Socket:
    """
    Start the server for max_retries times. (with a retry interval of retry_interval)
    If it fails to start, exit the program.

    host: The host to bind the socket to.
    port: The port to bind the socket to.
    max_connections: The maximum number of connections to listen for.
    max_retries: The maximum number of times to retry starting the server.
    retry_interval: The interval between retries.
    """

    for i in range(max_retries):
        server_socket = set_up_socket(host, port, max_connections)

        if server_socket is not None:
            return server_socket

        sleep(retry_interval)
        print(f"Retrying... ({i+1}/{max_retries})")

    print("Failed to start server after " + str(max_retries) + " retries. Exiting...")
    sys.exit(1)

def main() -> None:
    """
    The main function.
    """
    parser = argparse.ArgumentParser(description="Server for CS425 MP1")
    parser.add_argument("--host", type=str, help="The host to bind the socket to.")
    parser.add_argument("--port", type=str, help="The port to bind the socket to.")
    parser.add_argument('--server', action='store_true', help="whether to use the local server or the remote server")

    args = parser.parse_args()

    default_host = DEFAULT_SERVER_HOST if args.server else DEFAULT_LOCAL_HOST
    host = args.host if args.host else default_host
    port = int(args.port) if args.port else PORT

    print(f"Starting server on {host}:{port}")
    # start server
    server_socket = start_server(
        host=host,
        port=port,
        max_connections=MAX_CONNECTIONS,
        max_retries=START_MAX_RETRIES,
        retry_interval=RETRY_INTERVAL
    )

    # accept connections
    try:
        while True:
            handle_connection(server_socket, max_buffer_size = MAX_BUFFER_SIZE)
    # catch-all exception
    except Exception as exception:
        print(f"Error accepting connections. Exiting... {exception}")
        server_socket.close()
        sys.exit(1)
    except KeyboardInterrupt:
        print("Keyboard Interrupt. Exiting...")
        server_socket.close()
        sys.exit(1)

# MAIN
if __name__ == "__main__":
    main()
