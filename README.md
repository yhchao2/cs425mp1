# cs425mp1

Running xxx.py with --help to see the usage (if you are still unsure)

## START SERVER

Include --server if you want to start it in vm. (Otherwise it's on local machine)

```sh
python3 server.py --server
```

## START CLIENT

Include --server if you want to start it in vm. (Otherwise it's on local machine)

```sh
python3 client.py --server --command <command> --filename <filename>
```

Example

Don't include filename in the command
the filename, say if it's "./logs/vm.log", the client will try to access
"./logs/vm1.log", "./logs/vm2.log", "./logs/vm3.log", etc. or each different server

```sh
python3 client.py --server --command "grep -c GET" --filename ./logs/vm.log
```

## START TEST-CASE

Make sure package "rstr" is installed (it seems that 3.0.0 is okay, so ignore the version requirements in requirements.txt)
Make sure in the file 'test.py' you changed the CURRENT_SERVER_LIST = SERVER_LIST if you want run it in vm. 
(and CURRENT_SERVER_LIST = LOCAL_SERVER_LIST if you want to run it locally). 
**Make sure ./logs/test directory exist**, otherwise the test won't work as expected.


```sh
python3 test.py
```

## START PERF

Include --server if you want to start it in vm. (Otherwise it's on local machine)

```sh
python3 perf.py --server
```

## BATCH_START

Only for local test. Start all servers locally

```sh
python3 batch_start.py
```
