import os
import sys
import threading


ports = [8001, 8002, 8003, 8004, 8005, 8006, 8007, 8008, 8009, 8010]

def start_server(port):
    os.system(f"python3 server.py --port {port}")


if __name__ == "__main__":
    servers = []

    try:
        for port in ports:
            server_thread = threading.Thread(target=start_server, args=(port,))
            server_thread.start()
            servers.append(server_thread)

        for server in servers:
            server.join()
    except KeyboardInterrupt:
        print("Keyboard Interrupt. Exiting...")
        sys.exit(1)
