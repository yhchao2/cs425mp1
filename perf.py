from client import grep_command
import time
import argparse


SERVER_LIST = [
    ("fa23-cs425-7601.cs.illinois.edu", 8005, 1),
    ("fa23-cs425-7602.cs.illinois.edu", 8005, 2),
    ("fa23-cs425-7603.cs.illinois.edu", 8005, 3),
    ("fa23-cs425-7604.cs.illinois.edu", 8005, 4),
]

# LOCAL DEBUG SERVER LIST
LOCAL_SERVER_LIST = [
    ("localhost", 8001, 1),
    ("localhost", 8002, 2),
    ("localhost", 8003, 3),
    ("localhost", 8004, 4),
]

SAMPLE_SIZE = 10


def main():
    """
    Main function for the performance test.
    """
    parser = argparse.ArgumentParser(description='Performance test for the grep command.')
    parser.add_argument('--server', action='store_true', help='Run on the server.')

    args = parser.parse_args()

    server_list = SERVER_LIST if args.server else LOCAL_SERVER_LIST

    test_commands = [
        "grep -c GET",
        "grep -c DELETE",
        "grep -c Mac",
        "grep -c 200",
        "grep -c 4936",
        "grep -c 404"
    ]
    for command in test_commands:
        print(f"\n\n##### START COMMAND { command } #####\n\n")
        time_sum = 0
        for i in range(SAMPLE_SIZE):
            start_time = time.time()
            grep_command(server_list, command, "vm.log", 0)
            end_time = time.time()

            time_sum += end_time - start_time
            print(f"Sample {i}: {end_time - start_time}")

        print(f"Average time: {time_sum / SAMPLE_SIZE}")


if __name__ == "__main__":
    main()